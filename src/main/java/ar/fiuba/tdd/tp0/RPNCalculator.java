package ar.fiuba.tdd.tp0;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class RPNCalculator {

    // Interfaz para operaciones binarias basicas
    interface BinOperator {
        float exec(float fst, float snd);
    }

    // Operaciones binarias
    // Suma
    private static class Suma implements BinOperator {
        public float exec(float fst, float snd) {
            return fst + snd;
        }
    }

    private static class Resta implements BinOperator {
        public float exec(float fst, float snd) {
            return fst - snd;
        }
    }

    private static class Product implements BinOperator {
        public float exec(float fst, float snd) {
            return fst * snd;
        }
    }

    private static class Division implements BinOperator {
        public float exec(float fst, float snd) {
            return fst / snd;
        }
    }

    private static class Modulo implements BinOperator {
        public float exec(float fst, float snd) {
            return fst % snd;
        }
    }


    // Interfaz para operaciones generales de la calculadora
    interface StackOperation {
        void exec();
    }

    // Operacion especial de pushear un numero al stack
    private class PushNumber implements StackOperation {

        private Float store;

        public PushNumber(Float toPush) {
            this.store = toPush;
        }

        public void exec() {
            stack.push(this.store);
        }
    }

    // Stack para guardar los numeros y resultados
    private Stack<Float> stack = new Stack<Float>();
    // Hashmap para indexar las operaciones posibles
    private HashMap<String, StackOperation> ops = new HashMap<String, StackOperation>();

    // Cargamos las operaciones reales en el constructor de la calculadora
    public RPNCalculator() {

        // Operadores n-arios
        loadNOperator("++", new Suma());
        loadNOperator("--", new Resta());
        loadNOperator("**", new Product());
        loadNOperator("//", new Division());
        // Operadores binarios
        loadBinOperator("+", new Suma());
        loadBinOperator("-", new Resta());
        loadBinOperator("*", new Product());
        loadBinOperator("/", new Division());
        loadBinOperator("MOD", new Modulo());

    }

    private void loadNOperator(String token, BinOperator op) {

        ops.put(token, new StackOperation() {
            public void exec() {
                float accum = stackFold(op);
                stack.clear();
                stack.push(accum);
            }
        });

    }

    private void loadBinOperator(String token, BinOperator op) {

        ops.put(token, new StackOperation() {
            public void exec() {
                float snd = stack.pop();
                float fst = stack.pop();
                stack.push(op.exec(fst, snd));
            }
        });

    }

    private StackOperation parseToken(String token) {
        StackOperation op = ops.get(token);
        // No se me ocurre como 'evitar' este if
        return (op != null ? op : new PushNumber(Float.parseFloat(token)));
    }

    private Float stackFold(BinOperator op) {
        List<Float> list = new ArrayList<Float>(stack);
        float accum = list.get(0);
        list.remove(0);
        for (Float x : list) {
            accum = op.exec(accum, x);
        }
        return accum;
    }

    public Float eval(String expression) {
        //throw new RuntimeException("not implemented yet!");
        try {
            String[] tokens = expression.split(" ");
            for (String token : tokens) {
                parseToken(token).exec();
            }
            return stack.pop();
        } catch (NullPointerException | EmptyStackException | IndexOutOfBoundsException | NumberFormatException e) {
            throw new IllegalArgumentException();
        }
    }

}
